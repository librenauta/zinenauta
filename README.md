# zinenautas

>Zinenautas by librenauta, con parte de mucho código de Codedrops que pueden encontrar en https://tympanus.net/codrops/2018/11/12/page-flip-layout/ , gracias mil por compartir :3

Para la creación de los fanzines usé las siguientes fuentes y herramientas:
+imágenes de blogs
++https://idaloui.tumblr.com/
++https://binarynode.tumblr.com/
++https://himalayev.tumblr.com/ ♥
++https://aertime.tumblr.com/
++http://ilikethisart.net/ inspiración
++http://planetluke.com/
++https://kenaim.tumblr.com/
++https://akhmadbaev.tumblr.com/
++https://irrealbjctbyakhmadbaev.tumblr.com/ inspiración
++https://berrymetal.tumblr.com/
+vectores de mi laburo
+pens
++https://codepen.io/yoksel/pen/mjjyje
+photomosh [https://photomosh.com/] editar img live
+hydra, livecode visuals [https://hydra-editor.glitch.me]
+Imágenes de archillect [archillect.com]
+Tipografías de dafont
+Una monospace que copie de una web random
+Inkscape♥ [https://inkscape.org/es/]
+pdfunite
+gimp para contrastes a algunas imágenes
+atom(edit de template)
+fedora como OS
+mi escáner ♥
